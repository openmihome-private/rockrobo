=================
RockRobo JSON API
=================

.. contents:: Table of Contents

Overview
========

The *Xiaomi Mi Robot Vacuum*, internally referred to as *RockRobo*, is a home 
cleaning device. It is usually controlled with the proprietary *MiHome* smartphone app.

API
===

This JSON API is transported via the `MiHome Binary Protocol`_ on UDP/54321. 

.. _MiHome Binary Protocol: https://github.com/OpenMiHome/mihome-binary-protocol


Commands follow this pattern:
::

    {
        'id': 12345,
        'method': 'foo',
        'params': {
            'bar': 'xyz',
        }
    }

where 

* ``id`` is an integer sequence counter
* ``method`` denotes the method name
* ``params`` optional parameters

Responses follow this pattern:
::

    {
        'result': ["ok"],
        'id': 12345
    }

* ``result`` can be an object of any kind.

All strings are encoded as UTF-8. The object field order is rigid. The robot will ignore commands that are deviating from the fixed format.

Due to a bug(?) in the firmware, some JSON responses have a tailing ``0x00`` terminator. Remove it before you attempt to decode.

Initialization
--------------

miIO.config_router
~~~~~~~~~~~~~~~~~~

instructs the uninitialized robot to join a WLAN network.

Request:
::

    {
        'id': 12345,
        'method': 'miIO.config_router',
        'params':
        {
            'ssid': 'Your WiFi network',
            'passwd': 'Your WiFi password',
            'uid': XXX
        }
    }

* The ``uid`` parameter links the device to your *MiHome*-Account and, in turn, to your Android app. It can be ignored if you don't use the official app.
* ``params`` can be both an object or an array.

Response:
::

    {
        "result":["ok"],
        "id":12345
    }


Basic functions
---------------

app_name
~~~~~~~~
renames the device.


app_start
~~~~~~~~~
starts the regular cleaning cycle.

Request:
::

    {
        "id": 12345,
        "method":"app_start"
    }

Response:
::

    {
        "result": 0,
        "id": 12345
    }

app_pause
~~~~~~~~~
pauses the cleaning cycle.

Request:
::

    {
        'id': 12345,
        'method': 'app_pause',
    }

Response:
::

    {
        "result": 0, 
        "id": 12345
    }

The ``result`` seems to be 0 even when the operation is successful.

app_spot
~~~~~~~~
initiates the spot-cleaning cycle.


app_stop
~~~~~~~~
stops everything.

app_charge
~~~~~~~~~~
return to the base.

Request:
::

    {
        "id": 12345,
        "method":"app_charge"
    }

Response:
::

    {
        "result": 0,
        "id": 12345
    }


Remote control
--------------

app_rc_start
~~~~~~~~~~~~
begins a remote control sequence.

Request:
::

    {
        "id": 12345,
        "method": "app_rc_start"
    }

Response:
::

     {
        "result": 0,
        "id": 12345
     }

All following RC commands include a sequential counter called ``seqnum``, starting at ``1``.

app_rc_move
~~~~~~~~~~~
moves the robot.

Request:
::

    {
        "id": 12345,
        "method": "app_rc_move",
        "params": [
            {
                "omega": 0.0,
                "velocity": 0.1234,
                "seqnum": 123,
                "duration": 1500
           }
        ]
    }

* ``omega`` is an angle in radians.
* ``seqnum`` is the sequential counter for this RC session.
* ``duration`` is the time in milliseconds.

Response:
::

    {
        "result": 0,
        "id": 12345
    }

app_rc_end
~~~~~~~~~~
ends a remote control sequence.

Request:

::

    {
        "id": 12345,
        "method": "app_rc_end",
        "params": [{
            "seqnum": 123
        }]
    }

Response:

::

    {
        "result": 0,
        "id": 12345
    }

app_rc_stop
~~~~~~~~~~~
stops the robot.

Not observed yet.

app_rc_forward
~~~~~~~~~~~~~~
moves the robot forward.

Not observed yet.

app_rc_left
~~~~~~~~~~~
moves the robot to the left.

Not observed yet.

app_rc_right
~~~~~~~~~~~~
moves the robot to the right.

Not observed yet.

Timers
------

set_dnd_timer
~~~~~~~~~~~~~
configures `Do Not Disturb` time interval.

Request:
::

    {
        "id": 12345,
        "method": "set_dnd_timer",
        "params":[
            START_HOUR,
            START_MINUTE,
            END_HOUR,
            END_MINUTE
        ]
    }

* For example [22,0,8,0] would configure the `Do Not Disturb` timer from 22:00 to 08:00.

Response:
::

    {
        "result": 0,
        "id": 12345
    }

See also: `get_dnd_timer`_.


close_dnd_timer
~~~~~~~~~~~~~~~
disables the `Do Not Disturb` timer.

Request:
::

    {
        "id": 12345,
        "method": "close_dnd_timer",
        "params":[""]
    }

Response:
::

    {
        "result": 0, 
        "id": 12345
    }

upd_timer
~~~~~~~~~

Request:
::

    {
        "id": 12345,
        "method": "upd_timer",
        "params":["1487708102172","on"]
    }

* UNIX timestamp in milliseconds, as a string.
* ``"on"`` or ``"off"``

Response:
::

    {
        "result": 0, 
        "id": 12345
    }


Device state
------------

get_consumable
~~~~~~~~~~~~~~
returns the health of consumables like brushes.

Request:
::

    {
        "id": 12345,
        "method": "get_consumable"
    }

Response:
::

    {
        "result":
        [{
            "main_brush_work_time": 469,
            "side_brush_work_time": 469,
            "filter_work_time": 469
        }],
        "id": 12345
    }


get_clean_summary
~~~~~~~~~~~~~~~~~
not yet understood in detail.

Request:
::

    {
        "id": 12345,
        "method": "get_clean_summary"
    }

Response:
::

    {
        "result":
        [
            469,
            5472500,
            22,
            [
                1487808000,
                1487721600,
                1487289600,
                1487203200
            ]
        ],
        "id": 12345
    }

The list of UNIX timestamps (in seconds) is used as a key for individual clean records. See also:

* `get_clean_record`_


get_clean_record
~~~~~~~~~~~~~~~~
not yet understood in detail.

Request:
::

    {
        "id": 12345,
        "method": "get_clean_record",
        "params": [1487721600]
    }

The timestamp parameter is obtained from `get_clean_summary`_.

Response:
::

    {
        "result":
        [
            [ 1487720437, 1487764247, 9, 0, 0, 0 ],
            [ 1487720094, 1487720100, 6, 0, 0, 0 ],
            [ 1487719850, 1487720007, 15, 0, 0, 0 ],
            [ 1487712340, 1487712341, 1, 0, 0, 0 ],
        ],
        "id": 12345
    }'

This most likely represents the timestamped path of a cleaning cycle.


get_clean_record_map
~~~~~~~~~~~~~~~~~~~~

Not yet observed.


get_custom_mode
~~~~~~~~~~~~~~~

Not yet observed.


get_dnd_timer
~~~~~~~~~~~~~
Request:
::

    {
        "id": 12345,
        "method": "get_dnd_timer"
    }

Response:
::

    {
        "result":
        [{
            "start_hour": 22,
            "start_minute": 0,
            "end_hour": 8,
            "end_minute": 0,
            "enabled": 1
        }],
        "id": 12345
    }

See also: `set_dnd_timer`_.


get_gateway
~~~~~~~~~~~
Not yet observed. Most likely returns information on the device's Internet connection.

get_gateway_fail_msg
~~~~~~~~~~~~~~~~~~~~
Not yet observed. Most likely returns information on the device's Internet connection.

get_log_upload_status
~~~~~~~~~~~~~~~~~~~~~
Not yet understood.

Request:
::

    {
        "id": 12345,
        "method": "get_log_upload_status"
    }

Response:
::

    {
        "result": [ { "log_upload_status": 7 } ],
        "id": 12345
    }

get_map
~~~~~~~
Not observed in normal operation.

get_map_v1
~~~~~~~~~~
Not fully understood.

Request:
::

    {
        "id": 12345,
        "method": "get_map_v1"
    }

Response (Failure):
::

    {
        "result": [ "retry" ],
        "id": 12345
    }


get_serial_number
~~~~~~~~~~~~~~~~~
Not observed.


get_status
~~~~~~~~~~
return various device parameters.

Request:
::

    {
        "id": 12345,
        "method": "get_status"
    }

Response:
::

    {
        "result": [{
            "msg_ver": 3,
            "msg_seq": 50,
            "state": 8,
            "battery": 100,
            "clean_time": 9,
            "clean_area": 0,
            "error_code": 0,
            "map_present": 0,
            "in_cleaning": 0,
            "fan_power": 60,
            "dnd_enabled": 1
         }],
         "id": 12345
    }

* ``msg_ver``: unknown
* ``msg_seq``: unknown
* ``state``: unknown
* ``battery``: remaining battery capacity in percent
* ``clean_time``: current cycle duration in seconds
* ``clean_area``: current cycle cleaned area in m\ :sup:`2`
* ``error_code``: unknown boolean. ``0`` in normal operation.
* ``map_present``: boolean
* ``in_cleaning``: boolean, currently cleaning?
* ``fan_power``: fan power in percent
* ``dnd_enabled``: is DND enabled?

History
========
* 2017-02-22, Wolfgang Frisch
    Initial documentation.
